package org.drekorik.hw22;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

/**
 * Created by cloudjumper on 1/16/17.
 */
public class TestCalc {
    @Test
    public void testCalculator(){
        List<Item> items = Arrays.asList(
                new Item("Pen", 10), new Item("Clock", 100),
                new Item("Book", 100), new Item("Mouse", 1000),
                new Item("PC", 70000)
        );
        WorkSpace workSpace = new WorkSpace(items);
        assertThat(workSpace.count(), is(items.stream().mapToDouble(Item::getCost).sum()));
        System.out.println(workSpace.count());
    }
}
