package org.drekorik.hw24;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.StringJoiner;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

/**
 * Created by cloudjumper on 1/16/17.
 */
public class TestNoobSet {
    @Test
    public void testNoobSet(){
        List<ItemInterface> items = Arrays.asList(
                new Computer("PC2", 40000),
                new Pen("Pen2", 20, "Blue"),
                new Pen("Pen1", 20, "Black"),
                new AutomaticPen("AutoPen1", 30, "Blue", true),
                new Computer("PC1", 70000)
        );
        NoobSet noobSet = new NoobSet(items);
        System.out.println("New list: " + noobSet.toString());
        assertThat(noobSet.sortByValue().toString(),
                is(items.stream()
                        .sorted(Comparator.comparing(ItemInterface::getValue))
                        .collect(Collector.of(
                                () -> new StringJoiner(", ", "{", "}"),
                                (j, i) -> j.add(i.getName() + ": " + i.getValue()),
                                StringJoiner::merge,
                                StringJoiner::toString
                        ))
                )
        );
        System.out.println("Sorted by value: " + noobSet.sortByValue().toString());

        assertThat(noobSet.sortByName().toString(),
                is(items.stream()
                        .sorted(Comparator.comparing(ItemInterface::getName))
                        .collect(Collector.of(
                                () -> new StringJoiner(", ", "{", "}"),
                                (j, i) -> j.add(i.getName() + ": " + i.getValue()),
                                StringJoiner::merge,
                                StringJoiner::toString
                        ))
                )
        );
        System.out.println("Sorted by name" + noobSet.sortByName().toString());

        items = Arrays.asList(
                new Computer("PC2", 40000),
                new Pen("Pen2", 20, "Blue"),
                new Pen("Pen1", 20, "Black"),
                new AutomaticPen("AutoPen1", 30, "Blue", true),
                new Computer("PC1", 40000)
        );
        noobSet = new NoobSet(items);
        System.out.println("New list: " + noobSet.toString());
        assertThat(noobSet.sortByValueAndName().toString(),
                is(items.stream()
                        .sorted((o1, o2) -> o1.getValue() != o2.getValue()
                                ?Double.compare(o1.getValue(), o2.getValue())
                                :o1.getName().compareTo(o2.getName()))
                        .collect(Collector.of(
                                () -> new StringJoiner(", ", "{", "}"),
                                (j, i) -> j.add(i.getName() + ": " + i.getValue()),
                                StringJoiner::merge,
                                StringJoiner::toString
                        ))
                )
        );
        System.out.println("Sorted by value and name: " + noobSet.sortByValueAndName().toString());
    }
}
