package org.drekorik.hw21;

import org.junit.Test;

//import static org.hamcrest.core.Is.is;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

/**
 * Created by cloudjumper on 1/16/17.
 */
public class TestPen {
    @Test
    public void testEqualsAndHashcode(){
        Pen pen1 = new Pen("Blue", false);
        Pen pen2 = new Pen("Blue", false);
        Pen pen3 = new Pen("Red", false);

        assertThat(pen1.equals(pen2), is(true));
        System.out.println("1-2 equals? " + pen1.equals(pen2));
        System.out.println("--- --- ---");

        assertThat(pen1.equals(pen3), is(false));
        System.out.println("1-3 equals? " + pen1.equals(pen3));

        System.out.println("--- --- ---");
        assertThat(pen1.hashCode(), is(pen2.hashCode()));
        System.out.println(pen1.toString());
        System.out.println(pen2.toString());
        System.out.println("--- --- ---");

        pen1.setColor("Red");
        assertThat(pen1.hashCode(), is(not(pen2.hashCode())));
        assertThat(pen1.hashCode(), is(pen3.hashCode()));
        System.out.println(pen1.toString());
        System.out.println(pen2.toString());
        System.out.println(pen3.toString());
    }
}
