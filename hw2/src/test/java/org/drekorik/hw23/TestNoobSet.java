package org.drekorik.hw23;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

/**
 * Created by cloudjumper on 1/16/17.
 */
public class TestNoobSet {
    @Test
    public void testNoobSet(){
        List<Item> items = Arrays.asList(
                new Pen("Pen1", 20, "Black"),
                new Pen("Pen2", 20, "Blue"),
                new AutomaticPen("AutoPen1", 30, "Blue", true),
                new Computer("PC1", 40000)
        );
        NoobSet noobSet = new NoobSet(items);
        assertThat(noobSet.toString(), is("{Pen1: 20.0, Pen2: 20.0, AutoPen1: 30.0, PC1: 40000.0}"));
        System.out.println(noobSet.toString());
    }
}
