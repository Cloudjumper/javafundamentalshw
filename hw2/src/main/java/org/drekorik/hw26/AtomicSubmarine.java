package org.drekorik.hw26;

/**
 * Created by cloudjumper on 12/26/16.
 */
public class AtomicSubmarine {
    public static void main(String[] args) {
        AtomicSubmarine submarine = new AtomicSubmarine();
        submarine.start();
    }

    public void start(){
        EngineForAtomicSubmarine engineForAtomicSubmarine = new EngineForAtomicSubmarine();
        engineForAtomicSubmarine.start();
        System.out.println("submarine move");
    }

    class EngineForAtomicSubmarine{
        private void start(){
            System.out.println("engine start");
        }
    }
}
