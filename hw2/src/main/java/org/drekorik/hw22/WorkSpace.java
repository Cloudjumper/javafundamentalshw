package org.drekorik.hw22;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by cloudjumper on 12/23/16.
 */
public class WorkSpace {
    private final List<Item> itemList;

    public WorkSpace(List<Item> itemList) {
        this.itemList = itemList;
    }

    public double count(){
        // Стараюсь быть в тренде
        return itemList.stream().mapToDouble(Item::getCost).sum();
    }
}
