package org.drekorik.hw22;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.experimental.Accessors;

/**
 * Created by cloudjumper on 12/23/16.
 */
@AllArgsConstructor
@Getter
public class Item {
    private String name;
    private double cost;
}
