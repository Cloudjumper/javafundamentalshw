package org.drekorik.hw21;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * Created by cloudjumper on 12/23/16.
 */
@AllArgsConstructor
@Accessors(chain = true)
public class Pen {
    @Setter
    @Getter
    private String color;

    @Getter
    private final Boolean automatic;

    @Override
    public boolean equals(Object o) {
        if (o == this) return true; //если тот же самый объект
        if (!(o instanceof Pen)) return false; //если не принадлежит классу
        final Pen other = (Pen) o; //кастим объект
        if (!other.canEqual((Object) this)) return false; //Вот это странно
        final Object this$color = this.color;
        final Object other$color = other.color;
        if (this$color == null ? other$color != null : !this$color.equals(other$color)) return false; //Проверяем уже по полям
        final Object this$automatic = this.automatic;
        final Object other$automatic = other.automatic;
        if (this$automatic == null ? other$automatic != null : !this$automatic.equals(other$automatic)) return false;
        return true;
    }

    @Override
    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $color = this.color;
        result = result * PRIME + ($color == null ? 43 : $color.hashCode());
        final Object $automatic = this.automatic;
        result = result * PRIME + ($automatic == null ? 43 : $automatic.hashCode());
        return result;
    }

    protected boolean canEqual(Object other) {
        return other instanceof Pen;
    }

    @Override
    public String toString() {
        return "Pen{" +
                "color='" + color + '\'' +
                ", automatic=" + automatic +
                ", hashCode=" + this.hashCode() +
                '}';
    }
}
