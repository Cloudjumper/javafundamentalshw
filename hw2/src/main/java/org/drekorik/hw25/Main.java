package org.drekorik.hw25;

import org.drekorik.hw25.db.Group;
import org.drekorik.hw25.db.Student;
import org.drekorik.hw25.enums.Course;
import org.drekorik.hw25.interfaces.IGroup;
import org.drekorik.hw25.interfaces.IStudent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by cloudjumper on 12/23/16.
 */
public class Main {
    public static void main(String[] args) {
        Map<Course, IGroup> groupMap = new HashMap<>();
        Map<String, IStudent> studentMap = new HashMap<>();
        for (Course course: Course.values()) {
            groupMap.put(course, course.isDouble()?new Group<Double>():new Group<Integer>());
        }

        for (int i=0; i<20; i++){
            studentMap.put("Stud" + i, new Student("Stud" + i));
        }

        groupMap.get(Course.Eng).addStudent(studentMap.get("Stud1"));
        groupMap.get(Course.Prog).addStudent(studentMap.get("Stud1"));
        //elvis worked wrong, I do not why
        if (Course.Eng.isDouble())
            groupMap.get(Course.Eng).addGrade(studentMap.get("Stud1"), new Double(5));
        else
            groupMap.get(Course.Eng).addGrade(studentMap.get("Stud1"), new Integer(5));

        if (Course.Prog.isDouble())
            groupMap.get(Course.Prog).addGrade(studentMap.get("Stud1"), new Double(5));
        else
            groupMap.get(Course.Prog).addGrade(studentMap.get("Stud1"), new Integer(5));

        if (Course.Prog.isDouble())
            groupMap.get(Course.Prog).addGrade(studentMap.get("Stud1"), new Double(4.5));
        else
            groupMap.get(Course.Prog).addGrade(studentMap.get("Stud1"), new Integer(4));

        Finder finder = new Finder(groupMap);

        System.out.println(finder.findGrades(studentMap.get("Stud1")));

        System.out.println(groupMap.get(Course.Eng).getGrade(studentMap.get("Stud1")));

        System.out.println(groupMap.toString());
    }
}
