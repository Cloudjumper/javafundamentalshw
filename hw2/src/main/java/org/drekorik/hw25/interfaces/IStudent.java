package org.drekorik.hw25.interfaces;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cloudjumper on 12/23/16.
 */
public interface IStudent {
    String getName();
}
