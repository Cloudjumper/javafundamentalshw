package org.drekorik.hw25.interfaces;

import org.drekorik.hw25.enums.Course;

import java.util.List;
import java.util.Map;

/**
 * Created by cloudjumper on 12/23/16.
 */
public interface IGroup<T extends Number> {
    void addStudent(IStudent student);
    void addGrade(IStudent student, T grade);
    void removeStudent(IStudent student);
    Map<IStudent, List<T>> getStudents();
    List<T> getGrade(IStudent student);
}
