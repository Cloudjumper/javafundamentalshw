package org.drekorik.hw25.interfaces;

import org.drekorik.hw25.enums.Course;

import java.util.List;
import java.util.Map;

/**
 * Created by cloudjumper on 12/26/16.
 */
public interface IFinder {
    Map<Course, List<Number>> findGrades(IStudent student);
}
