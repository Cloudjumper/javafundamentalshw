package org.drekorik.hw25.enums;

import org.drekorik.hw25.interfaces.IGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cloudjumper on 12/23/16.
 */
public enum Course {
    Math, Eng, Lit, Prog;

    public boolean isDouble(){
        switch (this){
            case Math:
            case Prog:
                return true;
            case Eng:
            case Lit:
                return false;
            default:
                return false;
        }
    }
}
