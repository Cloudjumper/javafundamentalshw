package org.drekorik.hw25;

import org.drekorik.hw25.enums.Course;
import org.drekorik.hw25.interfaces.IFinder;
import org.drekorik.hw25.interfaces.IGroup;
import org.drekorik.hw25.interfaces.IStudent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by cloudjumper on 12/26/16.
 */
public class Finder implements IFinder {
    private Map<Course, IGroup> groupMap;

    public Finder(Map<Course, IGroup> groupMap) {
        this.groupMap = groupMap;
    }

    @Override
    public Map<Course, List<Number>> findGrades(IStudent student) {
        Map<Course, List<Number>> res = new HashMap<>();
        for (Course course:groupMap.keySet()) {
            List<Number> tmp = groupMap.get(course).getGrade(student);
            if (tmp != null)
                res.put(course, tmp);
        }
        return res;
    }
}
