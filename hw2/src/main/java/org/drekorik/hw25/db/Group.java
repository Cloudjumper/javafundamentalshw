package org.drekorik.hw25.db;

import org.drekorik.hw25.interfaces.IGroup;
import org.drekorik.hw25.interfaces.IStudent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by cloudjumper on 12/23/16.
 */
public class Group<T extends Number> implements IGroup<T> {
    private final Map<IStudent, List<T>> students;

    public Group() {
        this.students = new HashMap<>();
    }

    @Override
    public void addStudent(IStudent student) {
        students.put(student, new ArrayList<>());
    }

    @Override
    public void addGrade(IStudent student, T grade) {
        students.get(student).add(grade);
    }

    @Override
    public void removeStudent(IStudent student) {
        students.remove(student);
    }

    @Override
    public Map<IStudent, List<T>> getStudents() {
        return new HashMap<>(students);
    }

    @Override
    public List<T> getGrade(IStudent student) {
        List<T> res = students.get(student);
        return res != null?new ArrayList<T>(res):null;
    }
}
