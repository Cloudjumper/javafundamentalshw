package org.drekorik.hw25.db;

import lombok.Getter;
import org.drekorik.hw25.interfaces.IStudent;

/**
 * Created by cloudjumper on 12/23/16.
 */
@Getter
public class Student implements IStudent {
    private final String name;

    public Student(String name) {
        this.name = name;
    }
}
