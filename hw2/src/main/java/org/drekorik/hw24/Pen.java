package org.drekorik.hw24;

/**
 * Created by cloudjumper on 12/23/16.
 */
public class Pen extends Item {
    protected String color;

    public Pen(String name, double value, String color) {
        this.name = name;
        this.value = value;
        this.color = color;
    }

    public String getColor() {
        return this.color;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public double getValue() {
        return this.value;
    }
}
