package org.drekorik.hw24;

/**
 * Created by cloudjumper on 12/23/16.
 */
public class Computer extends Item {

    public Computer(String name, double value){
        this.name = name;
        this.value = value;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public double getValue() {
        return this.value;
    }
}
