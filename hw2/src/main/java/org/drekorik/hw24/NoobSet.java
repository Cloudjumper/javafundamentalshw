package org.drekorik.hw24;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.StringJoiner;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * Created by cloudjumper on 12/23/16.
 */
public class NoobSet {
    private List<ItemInterface> items;

    public static void main(String[] args) {

    }

    public NoobSet(List<ItemInterface> items) {
        this.items = items;
    }

    public NoobSet sortByName() {
        this.items = this.items.stream()
                .sorted(Comparator.comparing(ItemInterface::getName))
                .collect(Collectors.toList());
        return this;
    }

    public NoobSet sortByValue() {
        this.items = this.items.stream()
                .sorted(Comparator.comparing(ItemInterface::getValue))
                .collect(Collectors.toList());
        return this;
    }

    public NoobSet sortByValueAndName() {
        this.items = this.items.stream()
                .sorted(
                        ((o1, o2) -> o1.getValue() != o2.getValue()
                                ? Double.compare(o1.getValue(), o2.getValue())
                                : o1.getName().compareTo(o2.getName()))
                )
                .collect(Collectors.toList());
        return this;
    }


    @Override
    public String toString() {
//        Collector<Item, StringJoiner, String> collector = new Collector<Item, StringJoiner, String>() {
//            @Override
//            public Supplier<StringJoiner> supplier() {
//                return new Supplier<StringJoiner>() {
//                    @Override
//                    public StringJoiner get() {
//                        return new StringJoiner(", ", "{", "}");
//                    }
//                };
//            }
//
//            @Override
//            public BiConsumer<StringJoiner, Item> accumulator() {
//                return new BiConsumer<StringJoiner, Item>() {
//                    @Override
//                    public void accept(StringJoiner stringJoiner, Item item) {
//                        stringJoiner.add(item.getName() + ": " + item.getValue());
//                    }
//                };
//            }
//
//            @Override
//            public BinaryOperator<StringJoiner> combiner() {
//                return new BinaryOperator<StringJoiner>() {
//                    @Override
//                    public StringJoiner apply(StringJoiner stringJoiner, StringJoiner stringJoiner2) {
//                        return stringJoiner.merge(stringJoiner2);
//                    }
//                };
//            }
//
//            @Override
//            public Function<StringJoiner, String> finisher() {
//                return new Function<StringJoiner, String>() {
//                    @Override
//                    public String apply(StringJoiner stringJoiner) {
//                        return stringJoiner.toString();
//                    }
//                };
//            }
//
//            @Override
//            public Set<Characteristics> characteristics() {
//                return new HashSet<>();
//            }
//        };
//        return this.items.stream().collect(collector);


        //Commended code above must say to you, that I know that it isn't magic
        return this.items.stream().collect(Collector.of(
                () -> new StringJoiner(", ", "{", "}"),
                (j, i) -> j.add(i.getName() + ": " + i.getValue()),
                StringJoiner::merge,
                StringJoiner::toString
        ));
    }
}
