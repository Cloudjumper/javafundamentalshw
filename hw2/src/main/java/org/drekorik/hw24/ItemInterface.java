package org.drekorik.hw24;

/**
 * Created by cloudjumper on 12/23/16.
 */
public interface ItemInterface {
    String getName();
    double getValue();
}
