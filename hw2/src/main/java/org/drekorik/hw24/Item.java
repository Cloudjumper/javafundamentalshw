package org.drekorik.hw24;

/**
 * Created by cloudjumper on 12/23/16.
 */
public abstract class Item implements ItemInterface{
    protected String name;
    protected double value;

    abstract public String getName();
    abstract public double getValue();
}
