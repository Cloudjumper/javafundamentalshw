package org.drekorik.hw23;

/**
 * Created by cloudjumper on 12/23/16.
 */
public abstract class Item {
    protected String name;
    protected double value;

    abstract public String getName();
    abstract public double getValue();

    public String toString() {
        return "org.drekorik.hw23.Item(name=" + this.getName() + ", value=" + this.getValue() + ")";
    }
}
