package org.drekorik.hw23;

/**
 * Created by cloudjumper on 12/23/16.
 */
public class AutomaticPen extends Pen {
    private boolean switcher;

    public AutomaticPen(String name, double value, String color, boolean switcher) {
        super(name, value, color);
        this.switcher = switcher;
    }

    public boolean isSwitcher() {
        return switcher;
    }
}
