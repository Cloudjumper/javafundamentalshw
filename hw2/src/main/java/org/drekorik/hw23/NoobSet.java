package org.drekorik.hw23;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * Created by cloudjumper on 12/23/16.
 */
public class NoobSet {
    private List<Item> items;

    public NoobSet(List<Item> items) {
        this.items = items;
    }

    @Override
    public String toString() {
//        Collector<Item, StringJoiner, String> collector = new Collector<Item, StringJoiner, String>() {
//            @Override
//            public Supplier<StringJoiner> supplier() {
//                return new Supplier<StringJoiner>() {
//                    @Override
//                    public StringJoiner get() {
//                        return new StringJoiner(", ", "{", "}");
//                    }
//                };
//            }
//
//            @Override
//            public BiConsumer<StringJoiner, Item> accumulator() {
//                return new BiConsumer<StringJoiner, Item>() {
//                    @Override
//                    public void accept(StringJoiner stringJoiner, Item item) {
//                        stringJoiner.add(item.getName() + ": " + item.getValue());
//                    }
//                };
//            }
//
//            @Override
//            public BinaryOperator<StringJoiner> combiner() {
//                return new BinaryOperator<StringJoiner>() {
//                    @Override
//                    public StringJoiner apply(StringJoiner stringJoiner, StringJoiner stringJoiner2) {
//                        return stringJoiner.merge(stringJoiner2);
//                    }
//                };
//            }
//
//            @Override
//            public Function<StringJoiner, String> finisher() {
//                return new Function<StringJoiner, String>() {
//                    @Override
//                    public String apply(StringJoiner stringJoiner) {
//                        return stringJoiner.toString();
//                    }
//                };
//            }
//
//            @Override
//            public Set<Characteristics> characteristics() {
//                return new HashSet<>();
//            }
//        };
//        return this.items.stream().collect(collector);


        //Commended code above must say to you, that I know that it isn't magic
        return this.items.stream().collect(Collector.of(
                () -> new StringJoiner(", ", "{", "}"),
                (j, i) -> j.add(i.getName() + ": " + i.getValue()),
                StringJoiner::merge,
                StringJoiner::toString
        ));
    }
}
