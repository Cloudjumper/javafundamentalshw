package org.drekorik.hw27;/**
 * Created by cloudjumper on 12/26/16.
 */

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
public @interface SubmarineType {
    String value();
}
