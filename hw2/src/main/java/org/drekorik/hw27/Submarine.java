package org.drekorik.hw27;

/**
 * Created by cloudjumper on 12/26/16.
 */
@SubmarineType("Atomic")
public class Submarine {
    public static void main(String[] args) {
        Submarine submarine = new Submarine();
        System.out.println(submarine.getClass().getDeclaredAnnotation(SubmarineType.class).value());
        submarine.start();
    }

    public void start(){
        EngineForSubmarine engineForSubmarine = new EngineForSubmarine();
        engineForSubmarine.start();
        System.out.println("submarine move");
    }

    class EngineForSubmarine {
        private void start(){
            System.out.println("engine start");
        }
    }
}
