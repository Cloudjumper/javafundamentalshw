package org.drekorik.hw31;

import org.hamcrest.core.Is;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

/**
 * Created by cloudjumper on 2/9/17.
 */
public class CrazyLoggerTest {
    @Test
    public void findAndGet() throws Exception {
        assertThat(CrazyLogger.findAndGet("Hello").size(), is(2));
    }

    @Test
    public void findAndPrint() throws Exception {
        CrazyLogger.findAndPrint("Hello");
    }

    @BeforeClass
    public static void put() throws Exception {
        CrazyLogger.put("Test1");
        CrazyLogger.put("Test2");
        CrazyLogger.put("Test3");
        CrazyLogger.put("Test4");
        CrazyLogger.put("Test5 Hello Test5");
        CrazyLogger.put("Test6HelloTest6");
    }

}