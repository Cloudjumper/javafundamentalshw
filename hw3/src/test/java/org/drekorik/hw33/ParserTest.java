package org.drekorik.hw33;

import org.hamcrest.core.Is;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

/**
 * Created by cloudjumper on 2/10/17.
 */
public class ParserTest {
    @Test
    public void isConsistently() throws Exception {
        assertTrue(!new Parser("/home/cloudjumper/tmp.html").isConsistently());
    }

    @Test
    public void sentencesWithLinks() throws Exception {
        List<String> res = new Parser("/home/cloudjumper/tmp.html").sentencesWithLinks();
        res.forEach(System.out::println);
        assertThat(res.size(), is(117));
    }

}