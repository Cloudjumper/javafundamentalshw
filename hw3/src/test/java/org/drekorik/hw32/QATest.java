package org.drekorik.hw32;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by cloudjumper on 2/9/17.
 */
public class QATest {
    @Test
    public void getQuestions0() throws Exception {
        System.out.println(new QA("ru").getQuestions());
    }

    @Test
    public void getQuestions1() throws Exception {
        System.out.println(new QA("en").getQuestions());
    }

    @Test
    public void getQuestions2() throws Exception {
        System.out.println(new QA().getQuestions());
    }

    @Test
    public void answer() throws Exception {
        System.out.println(new QA().answer(1));
    }

}