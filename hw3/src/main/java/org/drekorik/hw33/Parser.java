package org.drekorik.hw33;

import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by cloudjumper on 2/10/17.
 */
public class Parser {
    private BufferedReader reader;
    private String picP;
    private String sentencesP;

    public Parser(String file) throws FileNotFoundException {
        picP = "Рис\\. [0-9]+";
        sentencesP = "^.*" + picP +".*$";
        File file1 = new File(file);
        if (!file1.exists())
            throw new FileNotFoundException("No such file");
        reader = new BufferedReader(new FileReader(file1));
    }

    public Parser(String file, String pattern) throws FileNotFoundException {
        picP = pattern;
        sentencesP = "^.*" + picP +".*$";
        File file1 = new File(file);
        if (!file1.exists())
            throw new FileNotFoundException("No such file");
        reader = new BufferedReader(new FileReader(file1));
    }

    public boolean isConsistently() throws IOException {
        List<String> sentences = sentencesWithLinks();
        Pattern p = Pattern.compile(picP);
        Matcher matcher;
        LinkedList<Integer> res = new LinkedList<>();
        for (String sentence: sentences){
            matcher = p.matcher(sentence);
            if (matcher.find()) {
                res.add(Integer.parseInt(matcher.group(0).replace("Рис. ", "")));
                System.out.println(res.getLast());
                if (res.size()<2)
                    continue;
                if (res.getLast() - res.get(res.size()-2) != 1 && res.getLast() - res.get(res.size()-2) != 0){
                    System.out.println("BAD");
                    return false;
                }
            }
        }
        return true;
    }

    public List<String> sentencesWithLinks() throws IOException {
        String line;
        String[] sentences;
        List<String> res = new ArrayList<>();
        while (( line = reader.readLine()) != null){
            sentences = line
                    .replace("&nbsp;", "\"")
                    .replaceAll("</[a-zA-Z]+>", "")
                    .replaceAll("<[a-zA-Z]+>", "")
                    .split("(?<=[.?!;])\\s+(?=\\p{Lu})");
            for (String sentence : sentences){
                if (sentence.matches(sentencesP))
                    res.add(sentence);
            }
        }
        return res;
    }
}
