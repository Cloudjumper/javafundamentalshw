package org.drekorik.hw31;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by cloudjumper on 2/9/17.
 */
public class CrazyLogger {
    private static StringBuilder logger = new StringBuilder();

    public static List<String> findAndGet(String pattern){
        String[] logLines = logger.toString().split("\n");
        List<String> result = new ArrayList<>();
        for (String line : logLines){
            if (line.matches("^.*" + pattern + ".*$"))
                result.add(line);
        }
        return result;
    }

    public static void findAndPrint(String pattern){
        String[] logLines = logger.toString().split("\n");
        List<String> result = new ArrayList<>();
        for (String line : logLines){
            if (line.matches("^.*" + pattern + ".*$"))
                result.add(line);
        }
        result.forEach(System.out::println);
//        System.out.println(logger.toString().matches(""));
    }

    public static void put(String text) {
        if (!text.endsWith("\n"))
            text = text + "\n";
        logger.append(LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy : HH-mm")))
                .append(" - ")
                .append(text);
    }
}
