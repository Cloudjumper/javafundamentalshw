package org.drekorik.hw32;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

/**
 * Created by cloudjumper on 2/9/17.
 */
public class QA {
    private ResourceBundle qBundle;
    private ResourceBundle aBundle;

    public QA() {
        qBundle = ResourceBundle.getBundle("hw32/questions", new UTF8Control());
        aBundle = ResourceBundle.getBundle("hw32/answers", new UTF8Control());
    }

    public QA(String locale){
        Locale.setDefault(new Locale(locale));
        qBundle = ResourceBundle.getBundle("hw32/questions", new UTF8Control());
        aBundle = ResourceBundle.getBundle("hw32/answers", new UTF8Control());
    }

    public Map<String, String> getQuestions(){
        Map<String, String> res = new HashMap<>();
        for (String key : qBundle.keySet()){
            res.put(key, qBundle.getString(key));
        }
        return res;
    }

    public String answer(int qNumber){
        return aBundle.getString(Integer.toString(qNumber));
    }
}
