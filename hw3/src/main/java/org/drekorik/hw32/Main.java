package org.drekorik.hw32;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;
import java.util.MissingResourceException;

/**
 * Created by cloudjumper on 2/10/17.
 */
public class Main {
    public static void main(String[] args) throws IOException {
        boolean flag = true;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        QA qa = new QA();
        while (flag){
            System.out.println("Q number or change lang");
            String in = reader.readLine();
            if (in.equals("q")){
                flag = false;
                continue;
            }
            if (in.equals("lang")){
                System.out.println("lang?");
                qa = new QA(reader.readLine());
                continue;
            }
            try {
                System.out.println(qa.answer(Integer.parseInt(in)));
            } catch (MissingResourceException | NumberFormatException e){
                System.out.println("There is no question with this number");
                System.out.println("This questions are available:\n");
                Map<String, String> qs = qa.getQuestions();
                for (String q : qs.keySet()){
                    System.out.println(q + ": " + qs.get(q));
                }
            }
        }
    }
}
