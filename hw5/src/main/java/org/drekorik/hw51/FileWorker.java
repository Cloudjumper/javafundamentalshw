package org.drekorik.hw51;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by cloudjumper on 2/13/17.
 */
public class FileWorker {
    public enum Type{
        File, Dir
    }

    public static Map<Type, List<File>> getFilesAndDirectories(File path){
        List<File> files = new ArrayList<>();
        List<File> dirs = new ArrayList<>();
        try {
            for (File f : path.listFiles())
                if (!f.isHidden())
                    if (f.isFile())
                        files.add(f);
                    else
                        dirs.add(f);
        } catch (NullPointerException e){
            RuntimeException rt = new RuntimeException("No such directory");
            rt.addSuppressed(e);
            throw rt;
        }
        Map<Type, List<File>> result = new HashMap<>();
        result.put(Type.File, files);
        result.put(Type.Dir, dirs);
        return result;
    }

    public static Map<Type, List<Path>> getFilesAndDirectories(Path path){
        Map<Type, List<File>> get = getFilesAndDirectories(new File(path.toString()));
        Map<Type, List<Path>> res = new HashMap<>();
        get.forEach((type, files) -> res.put(type, files
                .stream()
                .map(File::getAbsolutePath)
                .map(Paths::get)
                .collect(Collectors.<Path>toList())));
        return res;
    }

    public static Map<Type, List<String>> getFilesAndDirectories(String path){
        Map<Type, List<File>> get = getFilesAndDirectories(new File(path));
        Map<Type, List<String>> res = new HashMap<>();
        get.forEach((type, files) -> res.put(type, files
                .stream()
                .map(File::getAbsolutePath)
                .collect(Collectors.<String>toList())));
        return res;
    }

    public static List<String> getFilesOrDirectories(String path, Type type){
        return getFilesAndDirectories(path).get(type);
    }

    public static List<Path> getFilesOrDirectories(Path path, Type type){
        return getFilesAndDirectories(path).get(type);
    }

    public static List<File> getFilesOrDirectories(File path, Type type){
        return getFilesAndDirectories(path).get(type);
    }

    public static void appendToFile(File file, String s){
        try (
                FileOutputStream fileOutputStream = new FileOutputStream(file, true);
                PrintStream printStream = new PrintStream(fileOutputStream)
        ) {
            printStream.println(s);
        } catch (IOException e) {
            RuntimeException rt = new RuntimeException("Can not write to this directory or this file");
            rt.addSuppressed(e);
            throw rt;
        }
    }

    public static void appendToFile(Path file, String s){
        try {
            Files.write(file, s.getBytes(), StandardOpenOption.APPEND, StandardOpenOption.CREATE);
        } catch (IOException e) {
            RuntimeException rt = new RuntimeException("Can not write to this directory or this file");
            rt.addSuppressed(e);
            throw rt;
        }
    }

    public static void writeToFile(File file, String s){
        try (
                FileOutputStream fileOutputStream = new FileOutputStream(file);
                PrintStream printStream = new PrintStream(fileOutputStream)
        ) {
            printStream.println(s);
        } catch (IOException e) {
            RuntimeException rt = new RuntimeException("Can not write to this directory or this file");
            rt.addSuppressed(e);
            throw rt;
        }
    }

    public static void writeToFile(Path file, String s){
        try {
            Files.write(file, s.getBytes(), StandardOpenOption.CREATE);
        } catch (IOException e) {
            RuntimeException rt = new RuntimeException("Can not write to this directory or this file");
            rt.addSuppressed(e);
            throw rt;
        }
    }

    public static String readFile(Path file){
        try {
            List<String> lines = Files.readAllLines(file);
            return lines.stream().collect(Collectors.joining("\n"));
        } catch (IOException e) {
            RuntimeException rt = new RuntimeException("Can not read this file");
            rt.addSuppressed(e);
            throw rt;
        }
    }

    public static String readFile(File file){
        try (
                FileInputStream fileInputStream = new FileInputStream(file);
                InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader)
        ) {
            return bufferedReader.lines().collect(Collectors.joining("\n"));
        } catch (IOException e) {
            RuntimeException rt = new RuntimeException("Can not read this file");
            rt.addSuppressed(e);
            throw rt;
        }
    }
}
