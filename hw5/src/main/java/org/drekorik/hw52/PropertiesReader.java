package org.drekorik.hw52;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.NoSuchFileException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * Created by cloudjumper on 2/13/17.
 */
public class PropertiesReader {
    private static Map<String, Properties> propertiesMap = new HashMap<>();

    public static String getProperty(String path, String key){
        if (!propertiesMap.containsKey(path)){
            System.out.println("Creating new Properties");
            Properties properties = new Properties();
            try (FileInputStream fileInputStream = new FileInputStream(path)) {
                // This works fine, bu suddenly stopped work by the reason which only god know!
//                properties.load(PropertiesReader.class.getResourceAsStream(path));
                properties.load(fileInputStream);
            } catch (IOException | NullPointerException e) {
                //Temporary
                System.out.println(e);
                return "No such property file";
            }
            propertiesMap.put(path, properties);
        }
        if (propertiesMap.get(path).getProperty(key) != null) {
            return propertiesMap.get(path).getProperty(key);
        }
        else {
            return "No such key/value pair";
        }
    }
}
