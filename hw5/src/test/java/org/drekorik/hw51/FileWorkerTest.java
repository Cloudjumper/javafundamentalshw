package org.drekorik.hw51;

import org.junit.Test;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

/**
 * Created by cloudjumper on 2/13/17.
 */
public class FileWorkerTest {
    @Test
    public void getFilesOrDirectories1() throws Exception {
        System.out.println(FileWorker.getFilesOrDirectories("/home/cloudjumper/", FileWorker.Type.File));
        System.out.println(FileWorker.getFilesOrDirectories("/home/cloudjumper/", FileWorker.Type.Dir));
    }

    @Test
    public void getFilesOrDirectories2() throws Exception {
        System.out.println(FileWorker.getFilesOrDirectories(Paths.get("/home/cloudjumper/"), FileWorker.Type.File));
        System.out.println(FileWorker.getFilesOrDirectories(Paths.get("/home/cloudjumper/"), FileWorker.Type.Dir));
    }

    @Test
    public void getFilesOrDirectories3() throws Exception {
        System.out.println(FileWorker.getFilesOrDirectories(new File("/home/cloudjumper/"), FileWorker.Type.File));
        System.out.println(FileWorker.getFilesOrDirectories(new File("/home/cloudjumper/"), FileWorker.Type.Dir));
    }

    @Test
    public void readFile1() throws Exception {
        System.out.println(FileWorker.readFile(new File("/home/cloudjumper/hw51.txt")));
    }

    @Test
    public void readFile2() throws Exception {
        System.out.println(FileWorker.readFile(Paths.get("/home/cloudjumper/hw51.txt")));
    }

    @Test
    public void appendToFile1() throws Exception {
        FileWorker.writeToFile(new File("/home/cloudjumper/hw51.txt"), "data1");
    }

    @Test
    public void appendToFile2() throws Exception {
        FileWorker.writeToFile(Paths.get("/home/cloudjumper/hw51.txt"), "data2");
    }

    @Test
    public void writeToFile1() throws Exception {
        FileWorker.appendToFile(new File("/home/cloudjumper/hw51.txt"), "data1");
    }

    @Test
    public void writeToFile2() throws Exception {
        FileWorker.appendToFile(Paths.get("/home/cloudjumper/hw51.txt"), "data2");
    }

    @Test
    public void getFilesAndDirectories1() throws Exception {
        Map<FileWorker.Type, List<String>> inDir = FileWorker.getFilesAndDirectories("/home/cloudjumper");
        System.out.println(inDir.get(FileWorker.Type.File).toString());
        System.out.println(inDir.get(FileWorker.Type.Dir).toString());
    }

    @Test
    public void getFilesAndDirectories2() throws Exception {
        Map<FileWorker.Type, List<File>> inDir = FileWorker.getFilesAndDirectories(new File("/home/cloudjumper"));
        System.out.println(inDir.get(FileWorker.Type.File).toString());
        System.out.println(inDir.get(FileWorker.Type.Dir).toString());
    }

    @Test
    public void getFilesAndDirectories3() throws Exception {
        Map<FileWorker.Type, List<Path>> inDir = FileWorker.getFilesAndDirectories(Paths.get("/home/cloudjumper"));
        System.out.println(inDir.get(FileWorker.Type.File).toString());
        System.out.println(inDir.get(FileWorker.Type.Dir).toString());
    }
}