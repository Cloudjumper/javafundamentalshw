package org.drekorik.hw52;

import org.junit.Test;

import java.io.File;
import java.nio.file.Paths;

import static org.junit.Assert.*;

/**
 * Created by cloudjumper on 2/13/17.
 */
public class PropertiesReaderTest {
    @Test
    public void getProperty1() throws Exception {
        System.out.println(PropertiesReader.getProperty("src/test/resources/hw52/property1.properties", "k1"));
        System.out.println(PropertiesReader.getProperty("src/test/resources/hw52/property1.properties", "k1"));
        System.out.println(PropertiesReader.getProperty("src/test/resources/hw52/property1.properties", "k2"));
        System.out.println(PropertiesReader.getProperty("src/test/resources/hw52/property2.properties", "k1"));
        System.out.println(PropertiesReader.getProperty("src/test/resources/hw52/property2.properties", "k2"));
        System.out.println(PropertiesReader.getProperty("src/test/resources/hw52/property2.properties", "k22"));
    }

    @Test
    public void getProperty2() throws Exception {
        System.out.println(PropertiesReader.getProperty("hw52/property1.properties", "k1"));
        System.out.println(PropertiesReader.getProperty("hw52/property1.properties", "k1"));
        System.out.println(PropertiesReader.getProperty("hw52/property1.properties", "k2"));
        System.out.println(PropertiesReader.getProperty("hw52/property2.properties", "k1"));
        System.out.println(PropertiesReader.getProperty("hw52/property2.properties", "k2"));
        System.out.println(PropertiesReader.getProperty("hw52/property2.properties", "k22"));
    }

}