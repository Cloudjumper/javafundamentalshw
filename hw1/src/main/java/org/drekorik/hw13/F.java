package org.drekorik.hw13;

import java.util.Arrays;

/**
 * Created by cloudjumper on 12/7/16.
 */
public class F {
        private int[] segment = {0, 20};
        private double step = 0.5;

    public static void main(String[] args) {
        F f;
        if (args.length < 2){
            f = new F();
            f.setSegment(new int[]{0, 100});
            f.setStep(Math.PI);
        } else {
            f = new F();
            f.setSegment(new int[]{Integer.parseInt(args[0]), Integer.parseInt(args[1])});
            f.setStep(Double.parseDouble(args[2]));
        }
        f.calc();
//        Is it how strictfp work? - -3.0000000000000004
    }

    public F(){

    }

    public void setSegment(int[] segment) {
        this.segment = segment;
    }

    public void setStep(double step) {
        this.step = step;
    }

    public void calc(){
        System.out.println(String.format("%1$-" + 5 + "s" + " --- " + "%2$" + 10 + "s", "x", "F(x)"));
        double point = this.segment[0];
        while (point < this.segment[1]){
            double res = func(point);
            System.out.println(String.format("%1$-" + 5 + "s" + " --- " + "%2$" + 10 + "s", point, res));
            point+=this.step;
        }
    }

    private strictfp double func(double point){
        return Math.tan(2*point) - 3;
    }
}
