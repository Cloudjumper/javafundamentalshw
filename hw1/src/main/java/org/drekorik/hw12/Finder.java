package org.drekorik.hw12;

import java.util.Arrays;
import java.util.Random;

/**
 * Created by cloudjumper on 12/7/16.
 */
public class Finder {
    private double[] array;
    private double condition;

    public static void main(String[] args) {
        Finder finder;
        if (args.length < 2){
            finder = new Finder(40);
            finder.setCondition(0.001);
        } else {
            finder = new Finder(Integer.parseInt(args[0]));
            finder.setCondition(Double.parseDouble(args[1]));
        }
        int res = finder.findByCondition();
        System.out.println("For coders: " + res);
        System.out.println("For humans: " + (res + 1));
        finder.printArray();
    }

    private Finder(int size) {
        this.array = new double[size];
        for (int i=0; i< this.array.length; i++){
            this.array[i] = 1/Math.pow((i + 1), 2);
        }
    }

    public void setCondition(double condition) {
        this.condition = condition;
    }

    public int findByCondition(){
        for (int i=0; i<this.array.length; i++){
            if (this.array[i] < this.condition){
                return i;
            }
        }
        return -1;
    }

    public void printArray(){
        System.out.println(Arrays.toString(this.array));
    }
}
