package org.drekorik.hw15;

import java.util.Arrays;

/**
 * Created by cloudjumper on 12/14/16.
 */
public class Main {
    public static void main(String[] args) {
        int[][] array;
        if (args.length < 1){
            array = new int[5][5];
        } else {
            array = new int[Integer.parseInt(args[0])][Integer.parseInt(args[0])];
        }
        for (int i = 0; i < array.length; i++){
            array[i][i] = 1;
            array[i][array.length - 1 - i] = 1;
        }
        for (int[] sub :
                array) {
            System.out.println(Arrays.toString(sub) + "\n");
        }
    }
}
