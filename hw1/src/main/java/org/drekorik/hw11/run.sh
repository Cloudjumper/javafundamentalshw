#!/bin/bash

pwd
if [ ! -d "hw1/build/selfmade" ]; then
  mkdir hw1/build
  mkdir hw1/build/selfmade
fi
javac -sourcepath hw1/src/main/java -d hw1/build/selfmade hw1/src/main/java/org/drekorik/hw11/Main.java
java -classpath hw1/build/selfmade org.drekorik.hw11.Main