package org.drekorik.hw14;

import java.util.Arrays;
import java.util.Random;

/**
 * Created by cloudjumper on 12/14/16.
 */
public class Max {
    private double[] array;
    public static void main(String[] args) {
        Max max;
        if (args.length < 1){
            max = new Max(21);
            max.printMaxSum();
        } else {
            max = new Max(Integer.parseInt(args[0]));
            max.printMaxSum();
        }
    }

    public Max(int length) {
        this.array = new double[length];
        Random random = new Random();
        for (int i = 0; i < this.array.length; i++)
            this.array[i] = random.nextDouble() + (random.nextInt(100) - 50);
    }

    public void printMaxSum(){
        if (this.array.length % 2 != 0){
            this.array = Arrays.copyOf(this.array, this.array.length + 1);

        }
        double max = 0;
        double tmp;
        for (int i = 0, j = array.length - 1; i < array.length / 2; i++, j--){
            tmp = this.array[i] + this.array[j];
            System.out.println(i + " + " + j + " = " + tmp);
            max = max < tmp?tmp:max;
        }
        System.out.println(max);
    }
}
