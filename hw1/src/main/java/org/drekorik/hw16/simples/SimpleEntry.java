package org.drekorik.hw16.simples;

import org.drekorik.hw16.interfaces.Entry;

/**
 * Created by cloudjumper on 12/19/16.
 */
public class SimpleEntry implements Entry {
    private String text;

    @Override
    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String getText() {
        return this.text;
    }
}
