package org.drekorik.hw16.simples;

import org.drekorik.hw16.interfaces.Entry;
import org.drekorik.hw16.interfaces.EntryList;

import java.util.Iterator;

/**
 * Created by cloudjumper on 12/19/16.
 */
public class SimpleEntryList implements EntryList {
    private Node head;
    private Node last;
    private int count;

    @Override
    public void add(Entry entry) {
        Node newNode;
        if (head == null){
            newNode = new Node(null, entry, null);
            head = newNode;
        } else {
            newNode = new Node(null, entry, last);
            last.nextEntry = newNode;
        }
        last = newNode;
        count++;
    }

    public Entry get(int index){
        if (index > count - 1)
            return null;
        Node node = head;
        for (int i = 0; i < index; i++){
            node = node.nextEntry;
        }
        return node.entry;
    }

    public void delete(int index){
        if (index > count -1)
            return;
        Node node = head;
        for (int i = 0; i < index; i++){
            node = node.nextEntry;
        }
        if (node.equals(last) && !node.equals(head)){
            node.prevEntry.nextEntry = null;
            last = node.prevEntry;
        } else if (!node.equals(last) && node.equals(head)){
            node.nextEntry.prevEntry = null;
            head = node.nextEntry;
        } else if (node.equals(last) && node.equals(head)){
            head = null;
            last = null;
        } else {
            node.prevEntry.nextEntry = node.nextEntry;
            node.nextEntry.prevEntry = node.prevEntry;
        }
        count--;
    }

    @Override
    public Entry[] toArray() {
        Entry[] array = new Entry[count];
        Node node = head;
        int i = 0;
        while (node != null){
            array[i++] = node.entry;
            node = node.nextEntry;
        }
        return array;
    }

    @Override
    public Iterator getIterator() {
        return null;
    }

    class Node{
        private Node nextEntry;
        private Entry entry;
        private Node prevEntry;

        public Node(Node nextEntry, Entry entry, Node prevEntry) {
            this.nextEntry = nextEntry;
            this.entry = entry;
            this.prevEntry = prevEntry;
        }
    }

    class SimpleEntryListIterator implements Iterator{
        @Override
        public boolean hasNext() {
            return false;
        }

        @Override
        public Object next() {
            return null;
        }
    }
}
