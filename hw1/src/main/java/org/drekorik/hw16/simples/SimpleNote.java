package org.drekorik.hw16.simples;


import org.drekorik.hw16.interfaces.Entry;
import org.drekorik.hw16.interfaces.EntryList;
import org.drekorik.hw16.interfaces.Note;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by cloudjumper on 12/7/16.
 */

/**
 * Implementation of {@link Note}, which use {@link Entry} as notes
 */
public class SimpleNote implements Note {
    /**
     * List of entries
     */
    private EntryList entryList = new SimpleEntryList();

    /**
     * Add new entry
     * @param entry any entry based on {@link Entry}
     */
    @Override
    public void addEntry(Entry entry) {
        entryList.add(entry);
    }

    /**
     * Delete entry by id
     * @param id entry number in list
     */
    @Override
    public void deleteEntry(int id) {
        entryList.delete(id);
    }

    /**
     * Replace text in entry
     * @param id entry number in list
     * @param text new text for entry
     */
    @Override
    public void editEntry(int id, String text) {
        entryList.get(id).setText(text);
    }

    /**
     * Get text in {@link String} format
     * @param id entry number in list
     * @return {@link Entry} with text
     */
    @Override
    public Entry getEntry(int id) {
        return entryList.get(id);
    }

    /**
     * Get list with entries
     * @return {@link List} of {@link Entry}
     */
    @Override
    public Entry[] getAllEntries() {
        return entryList.toArray();
    }
}
