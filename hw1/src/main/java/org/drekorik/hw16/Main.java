package org.drekorik.hw16;


import org.drekorik.hw16.wrappers.SimpleNoteWrapper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StreamCorruptedException;

/**
 * Created by cloudjumper on 12/7/16.
 */

/**
 * Main class
 */
public class Main {
    /**
     *
     * @param args
     */
    public static void main(String[] args) {
        SimpleNoteWrapper note = new SimpleNoteWrapper();
        String[] command;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        boolean work = true;
        while(work){
            try {
                System.out.println("Waiting for command. Type \"help\" for help");
//                Why limit 2 when I need 1 split? Coz when limit is 1 it will not be split at all.
                command = reader.readLine().split(" ", 2);
                switch (command[0].toLowerCase()){
                    case "help": {
                        System.out.println("add \"text\"    - add new entry");
                        System.out.println("list - get all entries");
                        System.out.println("get \"id\"    - get entry with index");
                        System.out.println("edit \"id\"   - edit entry with index");
                        System.out.println("delete \"id\" - delete entry with index");
                        System.out.println("exit    - close program");
                        break;
                    }
                    case "add": {
                        note.addEntry(command[1]);
                        break;
                    }
                    case "list": {
                        System.out.println(note.getAllEntries());
                        break;
                    }
                    case "get":{
                        System.out.println(note.getEntry(Integer.parseInt(command[1])));
                        break;
                    }
                    case "edit":{
                        note.editEntry(Integer.parseInt(command[1]), reader.readLine());
                        break;
                    }
                    case "delete":{
                        note.deleteEntry(Integer.parseInt(command[1]));
                        break;
                    }
                    case "exit":{
                        work = false;
                        break;
                    }
                    default:{
                        System.out.println("Wrong command.");
                        break;
                    }
                }
            } catch (IOException e){
                System.out.println("Read exception, closing program");
                break;
            }
        }
    }
}
