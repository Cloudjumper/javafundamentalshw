package org.drekorik.hw16.interfaces;

import java.util.List;

/**
 * Created by cloudjumper on 12/7/16.
 */

/**
 * Interface for any notes
 */
public interface Note {
    /**
     * Add new entry
     * @param entry any entry based on {@link Entry}
     */
    void addEntry(Entry entry);

    /**
     * Delete entry by id
     * @param id entry number in list
     */
    void deleteEntry(int id);

    /**
     * Replace text in entry
     * @param id entry number in list
     * @param text new text for entry
     */
    void editEntry(int id, String text);

    /**
     * Get text in {@link String} format
     * @param id entry number in list
     * @return {@link Entry} with text
     */
    Entry getEntry(int id);

    /**
     * Get list with entries
     * @return {@link List} of {@link Entry}
     */
    Entry[] getAllEntries();
}
