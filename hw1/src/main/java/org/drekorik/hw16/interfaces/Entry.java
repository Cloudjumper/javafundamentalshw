package org.drekorik.hw16.interfaces;

/**
 * Created by cloudjumper on 12/7/16.
 */

/**
 * Interface for any {@link Note} entry
 * @author drekorik
 */
public interface Entry {
    /**
     * Set text to a note
     * @param text any text in {@link String} format
     */
    void setText(String text);

    /**
     * Get entry text
     * @return text in {@link String} format
     */
    String getText();
}
