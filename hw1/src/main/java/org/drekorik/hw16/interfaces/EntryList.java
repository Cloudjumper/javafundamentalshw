package org.drekorik.hw16.interfaces;

import java.util.Iterator;

/**
 * Created by cloudjumper on 12/19/16.
 */
public interface EntryList {
    /**
     *
     */
    void add(Entry entry);

    /**
     *
     * @param index
     * @return
     */
    Entry get(int index);

    /**
     *
     * @param index
     */
    void delete(int index);

    /**
     *
     * @return
     */
    Entry[] toArray();

    /**
     *
     */
    Iterator getIterator();
}
