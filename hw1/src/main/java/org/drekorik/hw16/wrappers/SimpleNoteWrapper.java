package org.drekorik.hw16.wrappers;


import org.drekorik.hw16.interfaces.Entry;
import org.drekorik.hw16.interfaces.Note;
import org.drekorik.hw16.simples.SimpleEntry;
import org.drekorik.hw16.simples.SimpleNote;

import java.util.List;

/**
 * Created by cloudjumper on 12/7/16.
 */

/**
 * Wrapper around {@link SimpleNote}
 */
public class SimpleNoteWrapper {
    private Note note= new SimpleNote();

    /**
     * Convert {@link String } text to {@link Entry}, then add in {@link Note}
     * @param text
     */
    public void addEntry(String text){
        Entry entry = new SimpleEntry();
        entry.setText(text);
        note.addEntry(entry);
    }

    /**
     * Call delete method on {@link  Note}
     * @param id entry id in list
     */
    public void deleteEntry(int id){
        note.deleteEntry(id);
    }

    /**
     * Call edit method on {@link Note}
     * @param id entry id in list
     * @param text new note text
     */
    public void editEntry(int id, String text){
        note.editEntry(id, text);
    }

    /**
     *
     * @param id
     * @return
     */
    public String getEntry(int id){
        Entry entry = note.getEntry(id);
        if (entry == null){
            return "Not in range";
        }
        return entry.getText();
    }

    public String getAllEntries(){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(String.format("%1$-3s" + "   %2$s\n" , "id", "text"));
        Entry[] entries = note.getAllEntries();
        for (int i=0; i<entries.length; i++){
            stringBuilder.append(String.format("%1$-3d" + "   %2$s\n" , i, entries[i].getText()));
        }
        return stringBuilder.toString();
    }
}
