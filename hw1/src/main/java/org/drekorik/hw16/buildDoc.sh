#!/usr/bin/env bash

pwd
if [ ! -d "hw1/build/selfmadedoc" ]; then
  mkdir hw1/build
  mkdir hw1/build/selfmadedoc
fi
javadoc -sourcepath hw1/src/main/java interfaces -d hw1/build/selfmadedoc -subpackages org.drekorik.hw16