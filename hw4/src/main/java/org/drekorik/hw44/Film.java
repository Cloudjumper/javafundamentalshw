package org.drekorik.hw44;

import lombok.Builder;

import java.io.FileOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by cloudjumper on 2/10/17.
 */
public class Film implements Serializable {
    private String name = "None";
    private List<String> actors = new ArrayList<>();

    public String getName() {
        return name;
    }

    public Film setName(String name) {
        this.name = name;
        return this;
    }

    public List<String> getActors() {
        return actors;
    }

    public Film addActor(String actor) {
        this.actors.add(actor);
        return this;
    }
}
