package org.drekorik.hw44;

import java.io.*;

/**
 * Created by cloudjumper on 2/10/17.
 */
public class Serializer implements Serializable {
    public static void write(String file, Film film) throws IOException {
        try(
                FileOutputStream fileOutputStream = new FileOutputStream(file);
                ObjectOutputStream out = new ObjectOutputStream(fileOutputStream)
        ) {
            out.writeObject(film);
        }
    }
}
