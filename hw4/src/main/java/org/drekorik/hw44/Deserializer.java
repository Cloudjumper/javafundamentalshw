package org.drekorik.hw44;

import java.io.*;

/**
 * Created by cloudjumper on 2/10/17.
 */
public class Deserializer {
    public static Film read(String file) throws IOException, ClassNotFoundException {
        try(
                FileInputStream fileInputStream = new FileInputStream(file);
                ObjectInputStream in = new ObjectInputStream(fileInputStream)
        ) {
            return (Film) in.readObject();
        }
    }
}
