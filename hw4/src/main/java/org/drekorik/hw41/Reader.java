package org.drekorik.hw41;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.StringJoiner;

/**
 * Created by cloudjumper on 2/10/17.
 */
public class Reader {
    public static String read(String path) throws IOException {
        byte[] buf = new byte[20];
        try(FileInputStream in = new FileInputStream(path)) {
            StringJoiner joiner = new StringJoiner("");
            while (in.available() > 0){
                in.read(buf);
                joiner.add(new String(buf));
                Arrays.fill(buf, (byte) 0);
            }
            return joiner.toString();
        }
    }
}
