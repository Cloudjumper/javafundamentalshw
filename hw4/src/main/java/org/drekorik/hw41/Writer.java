package org.drekorik.hw41;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by cloudjumper on 2/10/17.
 */
public class Writer {
    public static void write(String str, String file) throws IOException {
        try(FileOutputStream out = new FileOutputStream(file)) {
            out.write(str.getBytes());
        }
    }
}
