package org.drekorik.hw42;

import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.StringJoiner;

/**
 * Created by cloudjumper on 2/10/17.
 */
public class Reader {
    public static String read(String path) throws IOException {
        char[] buf = new char[20];
        try(FileReader in = new FileReader(path)) {
            StringJoiner joiner = new StringJoiner("");
            while (in.read() > -1){
                in.read(buf);
                joiner.add(new String(buf));
                Arrays.fill(buf, (char) 0);
            }
            return joiner.toString();
        }
    }
}
