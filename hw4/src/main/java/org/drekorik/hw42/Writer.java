package org.drekorik.hw42;

import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by cloudjumper on 2/10/17.
 */
public class Writer {
    public static void write(String str, String file) throws IOException {
        try(FileWriter out = new FileWriter(file)) {
            out.write(str);
        }
    }
}
