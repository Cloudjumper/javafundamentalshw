package org.drekorik.hw43;

import lombok.SneakyThrows;

import java.io.*;

/**
 * Created by cloudjumper on 2/10/17.
 */
public class Writer {
    @SneakyThrows
    public static void write(String str, String file) throws IOException {
        try(
                FileOutputStream out = new FileOutputStream(file);
                OutputStreamWriter streamWriter = new OutputStreamWriter(out, "UTF16");
                BufferedWriter writer = new BufferedWriter(streamWriter)
        ) {
            writer.write(str);
        }
    }
}
