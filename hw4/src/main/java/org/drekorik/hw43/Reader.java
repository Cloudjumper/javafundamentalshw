package org.drekorik.hw43;

import java.io.*;
import java.util.Arrays;
import java.util.StringJoiner;

/**
 * Created by cloudjumper on 2/10/17.
 */
public class Reader {
    public static String read(String path) throws IOException {
        try(
                FileInputStream in = new FileInputStream(path);
                InputStreamReader streamReader = new InputStreamReader(in, "UTF8");
                BufferedReader reader = new BufferedReader(streamReader)
        ) {
            StringJoiner joiner = new StringJoiner("");
            String tmp;
            while ((tmp = reader.readLine()) !=null){
                joiner.add(tmp);
            }
            return joiner.toString();
        }
    }
}
