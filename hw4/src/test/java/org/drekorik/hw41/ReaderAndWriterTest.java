package org.drekorik.hw41;

import org.junit.Test;

import java.util.Arrays;
import java.util.StringJoiner;
import java.util.stream.Collector;

/**
 * Created by cloudjumper on 2/10/17.
 */
public class ReaderAndWriterTest {
    @Test
    public void readAndWrite() throws Exception {
        String file = Reader.read("src/test/resources/hw41/tmp.txt");
        System.out.println(file);
        String res = Arrays.stream(file.split(" "))
                .filter(ReaderAndWriterTest::isKeyword)
                .collect(Collector.of(() -> new StringJoiner(", "),
                        StringJoiner::add,
                        StringJoiner::merge,
                        StringJoiner::toString));
        Writer.write(res, "src/test/resources/hw41/res.txt");
    }

    public static boolean isKeyword(String word){
        final String keywords[] = { "abstract", "assert", "boolean",
                "break", "byte", "case", "catch", "char", "class", "const",
                "continue", "default", "do", "double", "else", "extends", "false",
                "final", "finally", "float", "for", "goto", "if", "implements",
                "import", "instanceof", "int", "interface", "long", "native",
                "new", "null", "package", "private", "protected", "public",
                "return", "short", "static", "strictfp", "super", "switch",
                "synchronized", "this", "throw", "throws", "transient", "true",
                "try", "void", "volatile", "while" };
        Arrays.sort(keywords);
        return Arrays.binarySearch(keywords, word) >= 0;
    }
}