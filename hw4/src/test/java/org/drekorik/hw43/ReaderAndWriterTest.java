package org.drekorik.hw43;

import org.hamcrest.core.Is;
import org.junit.Assert;
import org.junit.Test;

import java.io.*;
import java.util.Arrays;
import java.util.StringJoiner;
import java.util.stream.Collector;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * Created by cloudjumper on 2/10/17.
 */
public class ReaderAndWriterTest {
    @Test
    public void readAndWrite() throws Exception {
        String file = Reader.read("src/test/resources/hw43/tmp.txt");
//        System.out.println(file);
        String res = Arrays.stream(file.split(" "))
                .filter(ReaderAndWriterTest::isKeyword)
                .collect(Collector.of(() -> new StringJoiner(", "),
                        StringJoiner::add,
                        StringJoiner::merge,
                        StringJoiner::toString));
        assertTrue(Arrays.stream(res.split(", ")).allMatch(ReaderAndWriterTest::isKeyword));
        Writer.write(res, "src/test/resources/hw43/res.txt");
        BufferedReader reader = new BufferedReader(
                new InputStreamReader(
                        new FileInputStream("src/test/resources/hw43/res.txt"), "UTF16"));
        assertThat(reader.readLine(), is(res));
    }

    public static boolean isKeyword(String word){
        final String keywords[] = { "abstract", "assert", "boolean",
                "break", "byte", "case", "catch", "char", "class", "const",
                "continue", "default", "do", "double", "else", "extends", "false",
                "final", "finally", "float", "for", "goto", "if", "implements",
                "import", "instanceof", "int", "interface", "long", "native",
                "new", "null", "package", "private", "protected", "public",
                "return", "short", "static", "strictfp", "super", "switch",
                "synchronized", "this", "throw", "throws", "transient", "true",
                "try", "void", "volatile", "while" };
        Arrays.sort(keywords);
        return Arrays.binarySearch(keywords, word) >= 0;
    }
}