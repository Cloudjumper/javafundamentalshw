package org.drekorik.hw44;

import org.junit.Test;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * Created by cloudjumper on 2/10/17.
 */
public class SerializeAndDeserializeTest {
    @Test
    public void readAndWrite() throws Exception {
        Serializer.write("src/test/resources/hw44/res.out",
                new Film().addActor("Actor0").addActor("Actor1").setName("Name0"));
        Film film = Deserializer.read("src/test/resources/hw44/res.out");
        assertThat(film.getName(), is("Name0"));
        assertThat(film.getActors().toString(), is("[Actor0, Actor1]"));
        System.out.println(film.getName());
        System.out.println(film.getActors());
    }

    @Test
    public void read() throws IOException, ClassNotFoundException {
        Film film = Deserializer.read("src/test/resources/hw44/res.out");
        assertThat(film.getName(), is("Name0"));
        assertThat(film.getActors().toString(), is("[Actor0, Actor1]"));
        System.out.println(film.getName());
        System.out.println(film.getActors());
    }
}
